<?php

class CityController extends Controller
{
    public function actionList($parameters = [])
    {
        $country_id = (isset($parameters[0])) ? $parameters[0] : 0;
        $params['countries'] = $this->_countries;
        $params['cities'] = City::getCities($country_id);
        $params['country_id'] = $country_id;
        $this->display('list', $params);
    }
    
    public function actionView($parameters = [])
    {
        $params['city'] = new City($parameters[0]);
        
        $this->display('view', $params);
    }  
    
}
