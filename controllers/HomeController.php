<?php

class HomeController extends Controller
{
    public function actionIndex($parameters = [])
    {
        $params['countries'] = $this->_countries;
        $this->display('home', $params);
    }
    
    public function actionPage404($parameters = [])
    {
        $this->display('page404');     
    }
}
