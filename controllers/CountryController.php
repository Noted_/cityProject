<?php

class CountryController extends Controller
{
    public function actionList($parameters = [])
    {
        $params['countries'] = $this->_countries;
        $this->display('list', $params);
    }
}
