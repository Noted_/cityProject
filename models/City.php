<?php


class City extends Model
{ 
    
    public function __construct($city_id = 0) {
        if ($city_id > 0){
            $this->initObjectFromArray($this->getCityById($city_id));
            $response = file_get_contents("http://api.openweathermap.org/data/2.5/weather?id={$city_id}&appid=b5018676b6c9e7d01aa7056fd2b9186d");
            $this->weather = json_decode($response, true);
           
            
            $this->coords = json_decode($this->coord, true);
            $lon = $this->coords['lon'];
            $lat = $this->coords['lat'];
            $this->mapURL = "https://www.google.com/maps/embed/v1/view?key=AIzaSyAMrx_fDOlIvfap6laOjV0amkiLeB30z38&center={$lat},{$lon}&zoom=9";
            
        }
    }
    public static function getCities($country_id)
    {
        $where = ($country_id > 0) ? "WHERE country_id=?" : "";
        $sql = "SELECT * FROM city $where ORDER BY name ASC";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([$country_id]);
        return $stmt->fetchAll();
    }
    
    public static function getCityById($city_id)
    {
        $sql = "SELECT * FROM city WHERE city_id=?";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([$city_id]);
        return $stmt->fetch();
    }
    
    
}
