<?php


class Country extends Model
{
    
    public static function getCountries()
    {
        $sql = "SELECT * FROM country ORDER BY code ASC";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }
    
}
