<?php

class Controller 
{
    private $controllerId;
    private $view;
    protected $_countries;
    
    public function __construct($controllerId)
    {
        $this->controllerId = $controllerId;
        $this->_countries = Country::getCountries();
    }
    
    protected function display($file, $params = [])
    {
        $view = new View($this->controllerId . '/' . $file, $params);
        $content = $view->getHTML();
        $this->view = new View('index');
        $this->view->setParam('content', $content);
        $this->view->render();
    }
}
