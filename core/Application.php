<?php

class Application 
{
    private $_path;
    
    public function __construct()
    {
        
    }
    
    public function run()
    {
        $this->route();
        $this->action();
    }
    
    private function route()
    {
        $uri = trim($_SERVER['REQUEST_URI'], '/');
        $routes = include(ROOT . '/config/routes.php');
        $this->_path = 'home/page404';
        foreach ($routes as $key => $route) {
            if (preg_match("~^$key$~", $uri)) {
                $this->_path = preg_replace("~^$key$~", $route, $uri);
                break;
            }
        }
    }
    
    
    private function action()
    {
        $parts = explode('/', $this->_path);
        $controllerId = array_shift($parts);
        $controller = ucfirst($controllerId) . 'Controller';
        $action = 'action' . ucfirst( array_shift($parts) );
        $parameters = $parts;
        $controllerObject = new $controller($controllerId);
        $controllerObject->$action($parameters);
    }
    
}
