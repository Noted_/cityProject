<?php


class DataBase 
{
    private static $_connection;
    
    public static function connect()
    {
        try {
            $dsn = 'mysql:dbname=countries;host=localhost;charset=utf8';
            $username = 'root';
            $password = '';
            self::$_connection = new PDO($dsn, $username, $password);
            self::$_connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        } catch (PDOException $error) {
            echo $error->getMessage();
            die;
        }
    }
    
    public static function handler()
    {
        return self::$_connection;
    }
}
