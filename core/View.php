<?php

class View 
{
   protected $_file;
   protected $_params;
   
   public function __construct($file, $params = [])
   {
       $this->_file = ROOT . '/views/' . $file . '.php';
       $this->_params = $params;
   }
   
   public function setParam($name, $value)
   {
       $this->_params[$name] = $value;
   }

   public function getHTML()
   {
       extract($this->_params);
       ob_start();
       include($this->_file);
       $html = ob_get_contents();
       ob_end_clean();
       return $html;
   }
   
   public function render()
   {
       echo $this->getHTML();
   }
   
}
