<?php

return [
    ''                  => 'home/index',
    'cities'            => 'city/list',
    'cities/([\d]+)'    => 'city/list/$1',
    
    'city/([\d]+)'      => 'city/view/$1',
    'countries'         => 'country/list',
];
