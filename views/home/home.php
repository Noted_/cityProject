<div class="list-group">
        <div class="list-group-item list-group-item-success">Краiни</div>
        <?php foreach ($countries as $country): ?>
        <a class="list-group-item <?php if ($country_id == $country['country_id']): ?>active<?php endif; ?>" href="/cities/<?= $country['country_id'] ?>"><?= $country['name'] ?></a>
        <?php endforeach; ?>
</div> 