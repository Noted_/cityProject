<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="list-group">
            <div class="list-group-item list-group-item-success">Краiни</div>
            <?php foreach ($countries as $country): ?>
            <a class="list-group-item <?php if ($country_id == $country['country_id']): ?>active<?php endif; ?>" href="/cities/<?= $country['country_id'] ?>"><?= $country['name'] ?></a>
            <?php endforeach; ?>
        </div> 
    </div>
    <div class="col-xs-12 col-sm-6 col-md-8 col-lg-9">
        <div class="list-group">
            <div class="list-group-item list-group-item-success">Мiста</div>
            <?php foreach ($cities as $city): ?>
            <a href="/city/<?= $city['city_id'] ?>" class="list-group-item"><?= $city['name'] ?></a>
            <?php endforeach; ?>
        </div>        
    </div>
</div>


