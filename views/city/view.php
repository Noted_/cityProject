<div class="row">
    <div class="col-xs-12 col-md-6 col-offset-3">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <?= $city->name ?>
            </div>
            <div class="panel-body">
                <div class="list-group">
                    <div class="list-group-item">
                        Географiчна широта: <?= $city->coords[lon] ?>
                    </div>
                    <div id="map" class="list-group-item">
                        <iframe
                            accesskey=""
                            width="500"
                            height="450"
                            frameborder="0"
                            src= <?= $city->mapURL ?> allowfullscreen>
                        </iframe>
                    </div>
                    
                </div>
                
                
        </div>
            <div class="list-group pull-right">
                    <div class="list-grouop-item pull-right">    
                        Погода: <?= $city->weather["weather"][0]["description"] ?>
                        Температура: <?= $city->weather["main"]["temp"] - 273 ?>
                        Швидкість вітру: <?= $city->weather["wind"]["speed"] ?> м/с
                        Час сходу Сонця: <?= date("Y-m-d H:i:s", $city->weather["sys"]["sunrise"]) ; ?>
                        Час заходу Сонця: <?= date("Y-m-d H:i:s", $city->weather["sys"]["sunset"]) ; ?>
                    </div>
                    </div>
            <table class="table table-striped">
                <tr>
                    <td>Погода:</td>
                    <td><?= $city->weather["weather"][0]["description"] ?></td>
                </tr>
                <tr>
                    <td>Температура:</td>
                    <td><?= $city->weather["main"]["temp"] - 273 ?> градусів по Цельсію</td>
                </tr>
                <tr>
                    <td>Швидкість вітру:</td>
                    <td><?= $city->weather["wind"]["speed"] ?> м/с</td>
                </tr>
                <tr>
                    <td>Час сходу Сонця:</td>
                    <td><?= date("Y-m-d H:i:s", $city->weather["sys"]["sunrise"]) ; ?></td>
                </tr>
                <tr>
                    <td>Час заходу Сонця:</td>
                    <td><?= date("Y-m-d H:i:s", $city->weather["sys"]["sunset"]) ; ?></td>
                </tr>
            </table>    
    </div>
</div>
</div>