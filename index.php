<?php
    
    define('ROOT', __DIR__);
    
    spl_autoload_register(function($className) {
        $dirs = ['core', 'controllers', 'models'];
        foreach ($dirs as $dir) {
            $file = ROOT . '/' . $dir . '/' . $className . '.php';
            if (is_file($file)) {
                include($file);
                break;
            } 
        }
    });
    
    DataBase::connect();
    
    $app = new Application();
    $app->run();
    